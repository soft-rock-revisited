#ifndef HSV_H
#define HSV_H 1

#include "shader.h"

void hsv2rgb(struct vec3 *hsv, struct vec3 *rgb);

#endif
