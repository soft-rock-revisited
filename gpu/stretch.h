/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
RGB Stretch Shader
===================================================================== */

#ifndef STRETCH_H
#define STRETCH_H 1

#include "shader.h"

//======================================================================
// stretch shader data
struct stretch { struct shader shader;
  struct { GLint tex; GLint sub, mul; } uniform;
  struct { int   tex; struct vec3 sub, mul; } value;
  GLuint texture;
  int width;
  int height;
};

//======================================================================
// protoypes
struct stretch *stretch_init(struct stretch *stretch);
void stretch_reshape(struct stretch *stretch, int w, int h);
void stretch_display(struct stretch *stretch, GLuint fbo, GLuint texture, float *mi, float *ma);
void stretch_idle(struct stretch *stretch);

#endif
