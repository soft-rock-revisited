/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
FM-Oscillator Map Shader
===================================================================== */

#ifndef MAP_H
#define MAP_H 1

#include "config.h"
#include "shader.h"
#include "cmpsh.h"

//======================================================================
// map shader data
struct map { struct shader shader;
  struct { GLint phases_cur, phases_old; GLint sr, note_lo, note_hi, modi_lo, modi_hi; } uniform;
  struct { int   phases_cur, phases_old; float sr, note_lo, note_hi, modi_lo, modi_hi; } value;
  GLuint phase_textures[2][config_blocksize + 1];
  int phase_buffer[2];
  int width;
  int height;
  int win_width;
  int win_height;
  int frame;
  float note;
  float modi;
  float nrange;
  float mrange;
  float note_mouse;
  float modi_mouse;
  int samples[2];
  struct cmpsh cmpsh;
};

//======================================================================
// prototypes
void map_reset(struct map *map);
struct map *map_init(struct map *map);
void map_reshape(struct map *map, int w, int h, int winw, int winh);
GLuint map_display(struct map *map, GLuint fbo);
int map_keyspecial(struct map *map, int key, int x, int y);
int map_keynormal(struct map *map, int key, int x, int y);
int map_mouse(struct map *map, int button, int state, int x, int y);
int map_motion(struct map *map, int x, int y);

#endif
