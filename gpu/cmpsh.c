#include <stdlib.h>
#include <stdio.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include "cmpsh.h"
#include "cmpsh.frag.c"

struct cmpsh *cmpsh_init(struct cmpsh *cmpsh) {
  if (! cmpsh) { return 0; }
  if (! shader_init(&cmpsh->shader, 0, cmpsh_frag)) {
    return 0;
  }
  shader_uniform(cmpsh, tex0);
  shader_uniform(cmpsh, tex1);
  shader_uniform(cmpsh, tex2);
  shader_uniform(cmpsh, val);
  cmpsh->value.tex0 = 0;
  cmpsh->value.tex1 = 1;
  cmpsh->value.tex2 = 2;
  cmpsh->value.val.v[0] = 0;
  cmpsh->value.val.v[1] = 0;
  cmpsh->value.val.v[2] = 0;
  cmpsh->value.val.v[3] = 1;
  cmpsh->width  = config_tex_width;
  cmpsh->height = config_tex_height;
  cmpsh->which = 0;
  glGenTextures(2, &cmpsh->tex[0]);
  glEnable(GL_TEXTURE_2D);
  float *zero = calloc(1, sizeof(float) * 4 * cmpsh->width * cmpsh->height);
  for (int i = 0; i < 2; ++i) {
    glBindTexture(GL_TEXTURE_2D, cmpsh->tex[i]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, cmpsh->width, cmpsh->height, 0, GL_RGBA, GL_FLOAT, zero);
  }
  free(zero);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
  return cmpsh;
}

void cmpsh_reset(struct cmpsh *cmpsh) {
  float *one = calloc(1, sizeof(float) * 4 * cmpsh->width * cmpsh->height);
  for (int i = 0; i < 4 * cmpsh->width * cmpsh->height; ++i) {
    one[i] = 1.0;
  }
  glBindTexture(GL_TEXTURE_2D, cmpsh->tex[0]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, cmpsh->width, cmpsh->height, 0, GL_RGBA, GL_FLOAT, one);
  glBindTexture(GL_TEXTURE_2D, cmpsh->tex[1]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, cmpsh->width, cmpsh->height, 0, GL_RGBA, GL_FLOAT, one);
  glBindTexture(GL_TEXTURE_2D, 0);
  free(one);
}

GLuint cmpsh_display(struct cmpsh *cmpsh, GLuint fbo, GLuint tex1, GLuint tex2, struct vec4 *val) {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, cmpsh->width, cmpsh->height);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glFramebufferTexture2DEXT(
      GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
      GL_TEXTURE_2D, cmpsh->tex[1 - cmpsh->which], 0
  );
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, tex2);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex1);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, cmpsh->tex[cmpsh->which]);
  glUseProgramObjectARB(cmpsh->shader.program);
  shader_updatei(cmpsh, tex0);
  shader_updatei(cmpsh, tex1);
  shader_updatei(cmpsh, tex2);
  cmpsh->value.val.v[0] = val->v[0];
  cmpsh->value.val.v[1] = val->v[1];
  cmpsh->value.val.v[2] = val->v[2];
  cmpsh->value.val.v[3] = val->v[3];
  shader_updatef4(cmpsh, val);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glUseProgramObjectARB(0);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glFramebufferTexture2DEXT(
      GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
      GL_TEXTURE_2D, 0, 0
  );
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  cmpsh->which = 1 - cmpsh->which;
  return cmpsh->tex[cmpsh->which];
}
