/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Output Module
===================================================================== */

#ifndef OUTPUT_H
#define OUTPUT_H 1

#include "config.h"
#include "minmax.h"
#include "stretch.h"

//======================================================================
// output data structures

struct output {
  struct minmax minmax;
  struct stretch stretch;
  unsigned int frame;
  int width;
  int height;
  GLuint texture;
  int winwidth;
  int winheight;
};

//======================================================================
// prototypes

struct output *output_init(struct output *output);
void output_reshape(struct output *output, int ww, int hh, int w, int h);
void output_display(struct output *output, GLuint texture, GLuint fbo);

#endif
