/* =====================================================================
kitchen
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Utility Functions
===================================================================== */

#include <assert.h>
#include <math.h>
#include "util.h"

//======================================================================
// round 'x' up to the nearest power of two (which may be 'x' itself)
unsigned int roundtwo(unsigned int x) {
  assert(x <= 1u << 31u); // termination condition
  unsigned int y = 1;
  while (y < x) y <<= 1;
  return y;
}

//======================================================================
// find log2 of the nearest power of two above 'x'
unsigned int logtwo(unsigned int x) {
  assert(x <= 1u << 31u); // termination condition
  unsigned int y = 1, z = 0;
  while (y < x) { y <<= 1; z += 1; };
  return z;
}

float ftom(float f) {
  return log(f / 8.17579891564) / 0.0577622650;
}

float clamp(float f, float lo, float hi) {
  return fmaxf(lo, fminf(f, hi));
}

// EOF
