/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Main Module
===================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "revisited.h"

//======================================================================
// main module global mutable state
static struct revisited revisited;

//======================================================================
// main module initialization
int revisited_init() {
  memset(&revisited, 0, sizeof(revisited));
  for (int b = 0; b < config_beats; ++b) {
    if (! map_init(&revisited.map[b])) { return 0; }
    if (! colour_init(&revisited.colour[b])) { return 0; }
  }
  if (! output_init(&revisited.output)) { return 0; }
  glGenFramebuffersEXT(1, &revisited.fbo);
  revisited_reshape(config_win_width, config_win_height);
  if (! audio_init(&revisited.audio, &revisited.map[0])) { return 0; }
  revisited.starttime = 0;
  revisited.fullscreen = 0;
  return 1;
}

//======================================================================
// main module reshape callback
void revisited_reshape(int w, int h) {
  for (int b = 0; b < config_beats; ++b) {
    map_reshape(&revisited.map[b], config_tex_width, config_tex_height, w, h);
    colour_reshape(&revisited.colour[b], config_tex_width, config_tex_height);
  }
  output_reshape(&revisited.output, config_win_width, config_win_height, w, h);
}

//======================================================================
// main module display callback
void revisited_display() {
  revisited.beat = audio_beat;
  GLuint texs[config_beats];
  for (int m = 0; m < config_beats; ++m) {
    GLuint tex = map_display(&revisited.map[m], revisited.fbo);
    //colour_display(&revisited.colour[m], revisited.fbo, tex);
    texs[m] = tex; //revisited.colour[m].texture;
  }
  output_display(&revisited.output, texs[revisited.beat], revisited.fbo);
  glutSwapBuffers();
  glutReportErrors();
  revisited.frame += 1;
}

//======================================================================
// main module exit callback
void revisited_atexit(void) {
  double timeelapsed = (double) time(NULL) - (double) revisited.starttime;
  fprintf(stderr, "\n\n--------------------------------------------------------------------------\n");
  fprintf(stderr, "------------------------------- statistics -------------------------------\n");
  fprintf(stderr, "--------------------------------------------------------------------------\n");
  fprintf(stderr, "%10d seconds elapsed (+/- 1)\n", (int) timeelapsed);
  fprintf(stderr, "%10d frames rendered (%f fps)\n", revisited.frame, revisited.frame / timeelapsed);
  fprintf(stderr, "--------------------------------------------------------------------------\n");
}

//======================================================================
// main module idle callback
void revisited_idle() {
  if (revisited.starttime == 0) {
    revisited.starttime = time(NULL);
    revisited.frame = 0;
    atexit(revisited_atexit);
  }
  glutPostRedisplay();
}

void revisited_keynormal(unsigned char key, int x, int y) {
  switch (key) {
  case 27: // escape
    exit(0);  // FIXME
    break;
  default:
    map_keynormal(&revisited.map[revisited.beat], key, x, y);
    break;
  }
}

void revisited_keyspecial(int key, int x, int y) {
  map_keyspecial(&revisited.map[revisited.beat], key, x, y);
}

void revisited_mouse(int button, int state, int x, int y) {
  map_mouse(&revisited.map[revisited.beat], button, state, x, y);
}

void revisited_motion(int x, int y) {
  map_motion(&revisited.map[revisited.beat], x, y);
}

// EOF
