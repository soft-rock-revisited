#version 120
precision highp float;

uniform sampler2D tmin;
uniform sampler2D tmax;
uniform float dx;
uniform float dy;

void main() {
  vec4 min0 = texture2D(tmin, gl_TexCoord[0].xy + vec2(0.0, 0.0));
  vec4 min1 = texture2D(tmin, gl_TexCoord[0].xy + vec2(dx,  0.0));
  vec4 min2 = texture2D(tmin, gl_TexCoord[0].xy + vec2(0.0, dy ));
  vec4 min3 = texture2D(tmin, gl_TexCoord[0].xy + vec2(dx,  dy ));
  vec4 max0 = texture2D(tmax, gl_TexCoord[0].xy + vec2(0.0, 0.0));
  vec4 max1 = texture2D(tmax, gl_TexCoord[0].xy + vec2(dx,  0.0));
  vec4 max2 = texture2D(tmax, gl_TexCoord[0].xy + vec2(0.0, dy ));
  vec4 max3 = texture2D(tmax, gl_TexCoord[0].xy + vec2(dx,  dy ));
  gl_FragData[0] = min(min(min0, min1), min(min2, min3));
  gl_FragData[1] = max(max(max0, max1), max(max2, max3));
}
