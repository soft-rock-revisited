#version 120
precision highp float;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform vec4 val;

void main() {
  vec4 v0 = texture2D(tex0, gl_TexCoord[0].xy).rgba;
  vec2 p1 = texture2D(tex1, gl_TexCoord[0].xy).xy;
  vec2 p2 = texture2D(tex2, gl_TexCoord[0].xy).xy;
  vec2 d = p1 - p2;
  vec4 v = v0;
  if (dot(d,d) < v.w) {
    v = vec4(val.xyz, dot(d,d));
  }
  gl_FragData[0] = v;
}
