/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
False Colour Shader
===================================================================== */

#ifndef COLOUR_H
#define COLOUR_H 1

#include "shader.h"

//======================================================================
// false colour shader data
struct colour { struct shader shader;
  struct { GLint  statistic; GLint sr; } uniform;
  struct { int    statistic; float sr; } value;
  GLuint texture;
  int width;
  int height;
};

//======================================================================
// protoypes
struct colour *colour_init(struct colour *colour);
void colour_reshape(struct colour *colour, int w, int h);
void colour_display(struct colour *colour, GLuint fbo, GLuint texture);

#endif
