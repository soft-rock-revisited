/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
False Colour Shader
===================================================================== */

#include <stdio.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include "config.h"
#include "util.h"
#include "colour.h"
#include "colour.frag.c"

//======================================================================
// false colour shader initialization
struct colour *colour_init(struct colour *colour) {
  if (! colour) { return 0; }
  if (! shader_init(&colour->shader, 0, colour_frag)) {
    return 0;
  }
  shader_uniform(colour, statistic);
  shader_uniform(colour, sr);
  colour->value.statistic = 0;
  colour->value.sr = config_samplerate;
  glGenTextures(1, &colour->texture);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, colour->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glDisable(GL_TEXTURE_2D);
  colour->width  = 0;
  colour->height = 0;
  return colour;
}

//======================================================================
// false colour shader reshape callback
void colour_reshape(struct colour *colour, int w, int h) {
  colour->width  = roundtwo(w);
  colour->height = roundtwo(h);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, colour->texture);
  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, colour->width, colour->height,
    0, GL_RGBA, GL_FLOAT, 0
  );
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// false colour shader display callback
void colour_display(struct colour *colour, GLuint fbo, GLuint texture) {
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texture);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glFramebufferTexture2DEXT(
    GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
    colour->texture, 0
  );
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, colour->width, colour->height);
  glUseProgramObjectARB(colour->shader.program);
  shader_updatei(colour, statistic);
  shader_updatef(colour, sr);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glUseProgramObjectARB(0);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
}

// EOF
