/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Main Module
===================================================================== */

#ifndef REVISITED_H
#define REVISITED_H 1

#include <time.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include "config.h"
#include "audio.h"
#include "map.h"
#include "colour.h"
#include "output.h"

//======================================================================
// main module data
struct revisited {
  struct audio audio;
  struct map map[config_beats];
  struct colour colour[config_beats];
  struct output output;
  GLuint fbo;
  time_t starttime;
  unsigned int frame;
  int fullscreen;
  int beat;
};

//======================================================================
// prototypes
int revisited_init(void);
void revisited_reshape(int w, int h);
void revisited_display(void);
void revisited_atexit(void);
void revisited_idle(void);
void revisited_keynormal(unsigned char key, int x, int y);
void revisited_keyspecial(int key, int x, int y);
void revisited_mouse(int button, int state, int x, int y);
void revisited_motion(int x, int y);

#endif
