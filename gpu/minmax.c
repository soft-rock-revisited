/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Min/Max Shader
===================================================================== */

#include <stdio.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include "config.h"
#include "util.h"
#include "minmax.h"
#include "minmax.frag.c"

//======================================================================
// min/max shader initialization
struct minmax *minmax_init(struct minmax *minmax) {
  if (! minmax) { return 0; }
  if (! shader_init(&minmax->shader, 0, minmax_frag)) {
    return 0;
  }
  shader_uniform(minmax, tmin);
  shader_uniform(minmax, tmax);
  shader_uniform(minmax, dx);
  shader_uniform(minmax, dy);
  minmax->value.tmin = 0;
  minmax->value.tmax = 1;
  minmax->value.dx = 0;
  minmax->value.dy = 0;
  minmax->count  = 0;
  minmax->width  = 0;
  minmax->height = 0;
  minmax_reshape(minmax, config_tex_width, config_tex_height);
  return minmax;
}

//======================================================================
// false minmax shader reshape callback
void minmax_reshape(struct minmax *minmax, int w, int h) {
  minmax->width  = roundtwo(w);
  minmax->height = roundtwo(h);
  int oldcount = minmax->count;
  minmax->count = logtwo(roundtwo(w > h ? w : h));
  if (minmax->count != oldcount) {
    if (oldcount > 0) {
      glDeleteTextures(oldcount, &minmax->tmins[0]);
      glDeleteTextures(oldcount, &minmax->tmaxs[0]);
    }
    glGenTextures(minmax->count, &minmax->tmins[0]);
    glGenTextures(minmax->count, &minmax->tmaxs[0]);
    glEnable(GL_TEXTURE_2D);
    for (int i = 0; i < minmax->count; ++i) {
      glBindTexture(GL_TEXTURE_2D, minmax->tmins[i]);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, 1 << i, 1 << i, 0, GL_RGBA, GL_FLOAT, 0);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glBindTexture(GL_TEXTURE_2D, minmax->tmaxs[i]);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, 1 << i, 1 << i, 0, GL_RGBA, GL_FLOAT, 0);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
}

//======================================================================
// min/max shader display callback
void minmax_display(struct minmax *minmax, GLuint fbo, GLuint texture) {
  glEnable(GL_TEXTURE_2D);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, texture);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  GLuint buffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
  glDrawBuffers(2, &buffers[0]);
  glUseProgramObjectARB(minmax->shader.program);
  for (int i = minmax->count - 1; i >= 0; --i) {
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, minmax->tmins[i], 0);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_2D, minmax->tmaxs[i], 0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, 1 << i, 1 << i);
    minmax->value.dx = 1.0 / (1 << i);
    minmax->value.dy = 1.0 / (1 << i);
    shader_updatei(minmax, tmin);
    shader_updatei(minmax, tmax);
    shader_updatef(minmax, dx);
    shader_updatef(minmax, dy);
    glBegin(GL_QUADS); { glColor4f(1,1,1,1);
      glTexCoord2f(0, 0); glVertex2f(0, 0);
      glTexCoord2f(1, 0); glVertex2f(1, 0);
      glTexCoord2f(1, 1); glVertex2f(1, 1);
      glTexCoord2f(0, 1); glVertex2f(0, 1);
    } glEnd();
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, minmax->tmaxs[i]);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, minmax->tmins[i]);
  }
  glDrawBuffers(1, &buffers[0]);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_2D, 0, 0);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glUseProgramObjectARB(0);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, minmax->tmaxs[0]);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &minmax->maxpixel[0]);
  glBindTexture(GL_TEXTURE_2D, minmax->tmins[0]);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &minmax->minpixel[0]);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// min/max shader idle callback
void minmax_idle(struct minmax *minmax) { }

// EOF
