/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Min/Max Shader
===================================================================== */

#ifndef MINMAX_H
#define MINMAX_H 1

#include "shader.h"

//======================================================================
// min/max shader data
struct minmax { struct shader shader;
  struct { GLint tmin, tmax; GLint dx, dy; } uniform;
  struct { int   tmin, tmax; float dx, dy; } value;
  GLuint tmins[16];
  GLuint tmaxs[16];
  int count;
  int width;
  int height;
  float minpixel[4];
  float maxpixel[4];
};

//======================================================================
// protoypes
struct minmax *minmax_init(struct minmax *minmax);
void minmax_reshape(struct minmax *minmax, int w, int h);
void minmax_display(struct minmax *minmax, GLuint fbo, GLuint texture);
void minmax_idle(struct minmax *minmax);

#endif
