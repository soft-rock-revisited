/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
RGB Stretch Shader
===================================================================== */

#include <stdio.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include "config.h"
#include "util.h"
#include "stretch.h"
#include "stretch.frag.c"

//======================================================================
// rgb stretch shader initialization
struct stretch *stretch_init(struct stretch *stretch) {
  if (! stretch) { return 0; }
  if (! shader_init(&stretch->shader, 0, stretch_frag)) {
    return 0;
  }
  shader_uniform(stretch, tex);
  shader_uniform(stretch, sub);
  shader_uniform(stretch, mul);
  stretch->value.tex = 0;
  stretch->value.sub.v[0] = 0;
  stretch->value.sub.v[1] = 0;
  stretch->value.sub.v[2] = 0;
  stretch->value.mul.v[0] = 1;
  stretch->value.mul.v[1] = 1;
  stretch->value.mul.v[2] = 1;
  glGenTextures(1, &stretch->texture);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, stretch->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glDisable(GL_TEXTURE_2D);
  stretch->width  = 0;
  stretch->height = 0;
  return stretch;
}

//======================================================================
// false stretch shader reshape callback
void stretch_reshape(struct stretch *stretch, int w, int h) {
  stretch->width  = roundtwo(w);
  stretch->height = roundtwo(h);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, stretch->texture);
  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, stretch->width, stretch->height,
    0, GL_RGBA, GL_FLOAT, 0
  );
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// false stretch shader display callback
void stretch_display(struct stretch *stretch, GLuint fbo, GLuint texture, float *mi, float *ma) {
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texture);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glFramebufferTexture2DEXT(
    GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
    stretch->texture, 0
  );
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, stretch->width, stretch->height);
  glUseProgramObjectARB(stretch->shader.program);
  stretch->value.sub.v[0] = mi[0];
  stretch->value.sub.v[1] = mi[1];
  stretch->value.sub.v[2] = mi[2];
  stretch->value.mul.v[0] = 1.0 / (ma[0] - mi[0]);
  stretch->value.mul.v[1] = 1.0 / (ma[1] - mi[1]);
  stretch->value.mul.v[2] = 1.0 / (ma[2] - mi[2]);
  shader_updatei(stretch, tex);
  shader_updatef3(stretch, sub);
  shader_updatef3(stretch, mul);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glUseProgramObjectARB(0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glBindTexture(GL_TEXTURE_2D, stretch->texture);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// false stretch shader idle callback
void stretch_idle(struct stretch *stretch) { }

// EOF
