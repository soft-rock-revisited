/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
FM-Oscillator Map Shader
===================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include "config.h"
#include "audio.h"
#include "util.h"
#include "hsv.h"
#include "map.h"
#include "map.frag.c"

void map_reset(struct map *map) {
  float *zero = calloc(1, sizeof(float) * 4 * map->width * map->height);
  glEnable(GL_TEXTURE_2D);
  for (int j = 0; j < 2; ++j) {
    map->samples[j] = 0;
    map->phase_buffer[j] = 0;
    for (int i = 0; i < config_blocksize + 1; ++i) {
      glBindTexture(GL_TEXTURE_2D, map->phase_textures[j][i]);
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, map->width, map->height, GL_RGBA, GL_FLOAT, zero);
    }
  }
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
  free(zero);
  cmpsh_reset(&map->cmpsh);
}

struct map *map_init(struct map *map) {
  if (! map) { return 0; }
  if (! cmpsh_init(&map->cmpsh)) { return 0; }
  if (! shader_init(&map->shader, 0, map_frag)) {
    return 0;
  }
  shader_uniform(map, phases_cur);
  shader_uniform(map, phases_old);
  shader_uniform(map, sr);
  shader_uniform(map, note_lo);
  shader_uniform(map, note_hi);
  shader_uniform(map, modi_lo);
  shader_uniform(map, modi_hi);
  map->value.phases_cur = 0;
  map->value.phases_old = 1;
  map->value.sr = config_samplerate;
  map->note   = 29.981804;
  map->modi   = 52.463516;
  map->nrange = 32.0;
  map->mrange = 32.0;
  map->width  = config_tex_width;
  map->height = config_tex_height;

  glGenTextures(config_blocksize + 1, map->phase_textures[0]);
  glGenTextures(config_blocksize + 1, map->phase_textures[1]);
  glEnable(GL_TEXTURE_2D);
  float *zero = calloc(1, sizeof(float) * 4 * map->width * map->height);
  for (int j = 0; j < 2; ++j) {
    for (int i = 0; i < config_blocksize + 1; ++i) {
      glBindTexture(GL_TEXTURE_2D, map->phase_textures[j][i]);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, map->width, map->height, 0, GL_RGBA, GL_FLOAT, zero);
    }
  }
  free(zero);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);

  map_reset(map);
  return map;
}

void map_reshape(struct map *map, int w, int h, int winw, int winh) {
  map->width  = roundtwo(w);
  map->height = roundtwo(h);
  map->win_width = winw;
  map->win_height = winh;
  map_reset(map);
}

GLuint map_display(struct map *map, GLuint fbo) {
  GLuint retval = 0;
  for (int i = 0; i < config_overdrive; ++i) {
    int settled = 1;//map->samples[0] > 48000;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, map->width, map->height);
    glUseProgramObjectARB(map->shader.program);
    shader_updatei(map, phases_cur);
    shader_updatei(map, phases_old);
    shader_updatef(map, sr);
    map->value.note_lo = map->note - config_aspect * map->nrange;
    map->value.note_hi = map->note + config_aspect * map->nrange;
    map->value.modi_lo = map->modi - map->mrange;
    map->value.modi_hi = map->modi + map->mrange;
    shader_updatef(map, note_lo);
    shader_updatef(map, note_hi);
    shader_updatef(map, modi_lo);
    shader_updatef(map, modi_hi);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    for (int j = 0; j < 2; ++j) {
      for (int k = 0; k <= j * settled; ++k) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, map->phase_textures[j][(map->phase_buffer[j] + 2) % (config_blocksize + 1)]);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, map->phase_textures[j][map->phase_buffer[j]]);
        glFramebufferTexture2DEXT(
          GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
          GL_TEXTURE_2D, map->phase_textures[j][(map->phase_buffer[j] + 1) % (config_blocksize + 1)], 0
        );
        glBegin(GL_QUADS); { glColor4f(1,1,1,1);
          glTexCoord2f(0, 0); glVertex2f(0, 0);
          glTexCoord2f(1, 0); glVertex2f(1, 0);
          glTexCoord2f(1, 1); glVertex2f(1, 1);
          glTexCoord2f(0, 1); glVertex2f(0, 1);
        } glEnd();
        map->phase_buffer[j] = (map->phase_buffer[j] + 1) % (config_blocksize + 1);
        map->samples[j] += 1;
        glFramebufferTexture2DEXT(
          GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
          GL_TEXTURE_2D, 0, 0
        );
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);
      }
    }
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    glUseProgramObjectARB(0);
    if (settled) {
      float o = map->samples[1] - map->samples[0];
      struct vec3 hsv;
      hsv.v[0] = (sqrt(5.0) - 1.0)/2.0 * o / 480;
      hsv.v[1] = 1.0;
      hsv.v[2] = 1.0;//clamp(o / 16.0, 0.0, 1.0);
      struct vec3 rgb;
      hsv2rgb(&hsv, &rgb);
      struct vec4 rgba;
      rgba.v[0] = rgb.v[0];
      rgba.v[1] = rgb.v[1];
      rgba.v[2] = rgb.v[2];
      rgba.v[3] = 1.0;
      retval = cmpsh_display(&map->cmpsh, fbo,
        map->phase_textures[0][map->phase_buffer[0]],
        map->phase_textures[1][map->phase_buffer[1]],
        &rgba
      );
    }
  }
  return retval;
}

int map_keyspecial(struct map *map, int key, int x, int y) {
  switch (key) {
    case GLUT_KEY_UP:    map->modi += map->mrange * 0.1; break;
    case GLUT_KEY_DOWN:  map->modi -= map->mrange * 0.1; break;
    case GLUT_KEY_LEFT:  map->note -= map->nrange * 0.1; break;
    case GLUT_KEY_RIGHT: map->note += map->nrange * 0.1; break;
    case GLUT_KEY_PAGE_UP:    map->mrange *= 0.5; break;
    case GLUT_KEY_PAGE_DOWN:  map->mrange /= 0.5; break;
    case GLUT_KEY_HOME:       map->nrange *= 0.5; break;
    case GLUT_KEY_END:        map->nrange /= 0.5; break;
    default: return 0;
  }
  map_reset(map);
  return 1;
}

int map_keynormal(struct map *map, int key, int x, int y) {
  switch (key) {
    case 's':
      map->nrange *= 0.9726549474122855;
      map->mrange *= 0.9726549474122855;
      system("import -window srrv \"srrv-`date --iso=s | tr \":\" \"-\"`.png\"");
      map_reset(map);
    case ' ': fprintf(stderr, "%f %f @ %f %f\n", map->note, map->modi, map->nrange, map->mrange); break;
    default: return 0;
  }
  return 1;
}

int map_mouse(struct map *map, int button, int state, int x, int y) {
  if (state == GLUT_DOWN) {
    float u =       (float)  x      / (float) map->win_width;
    float v = 1.0 - (float) (y + 1) / (float) map->win_height;
    float note
      =     (map->note - config_aspect * map->nrange) * (1 - u)
      + u * (map->note + config_aspect * map->nrange);
    float modi = (map->modi - map->mrange) * (1 - v) + v * (map->modi + map->mrange);
    float factor = 1.0;
    switch (button) {
      case GLUT_MIDDLE_BUTTON:
        map->note = note;
        map->modi = modi;
        map_reset(map);
        return 1;
      case GLUT_LEFT_BUTTON:
      case 3:
        factor = sqrt(0.5);
        break;
      case GLUT_RIGHT_BUTTON:
      case 4:
        factor = sqrt(2.0);
        break;
      default:
        return 0;
    }
    map->note = note + (map->note - note) * factor;
    map->modi = modi + (map->modi - modi) * factor;
    map->nrange *= factor;
    map->mrange *= factor;
    map_reset(map);
    fprintf(stderr, "\t%f\t%f\n", map->note, map->modi);
    return 1;
  }
  return 0;
}

int map_motion(struct map *map, int x, int y) {
  if (x < 0 || map->win_width <= x || y < 0 || map->win_height <= y) {
    return 0;
  } else {
    float u =       (float)  x      / (float) map->win_width;
    float v = 1.0 - (float) (y + 1) / (float) map->win_height;
    float note
      =     (map->note - config_aspect * map->nrange) * (1 - u)
      + u * (map->note + config_aspect * map->nrange);
    float modi = (map->modi - map->mrange) * (1 - v) + v * (map->modi + map->mrange);
    map->note_mouse = note;
    map->modi_mouse = modi;
    fprintf(stderr, "\t%f\t%f\r", map->note_mouse, map->modi_mouse);
    return 1;
  }
}

// EOF
