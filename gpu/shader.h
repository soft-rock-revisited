/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Generic Shader
===================================================================== */

#ifndef SHADER_H
#define SHADER_H 1

#include <GL/glew.h>

//======================================================================
// generic shader data
struct shader {
  GLint linkStatus;
  GLhandleARB program;
  GLhandleARB fragment;
  GLhandleARB vertex;
  const GLcharARB *fragmentSource;
  const GLcharARB *vertexSource;
};

struct vec4 {
  float v[4];
};

struct vec3 {
  float v[3];
};

struct vec2 {
  float v[2];
};

//======================================================================
// generic shader uniform location access macro
#define shader_uniform(self,name) \
  (self)->uniform.name = \
     glGetUniformLocationARB((self)->shader.program, #name)

//======================================================================
// generic shader uniform update access macro (integer)
#define shader_updatei(self,name) \
  glUniform1iARB((self)->uniform.name, (self)->value.name)

//======================================================================
// generic shader uniform update access macro (float)
#define shader_updatef(self,name) \
  glUniform1fARB((self)->uniform.name, (self)->value.name)

//======================================================================
// generic shader uniform update access macro (float3)
#define shader_updatef3(self,name) \
  glUniform3fARB((self)->uniform.name, (self)->value.name.v[0], (self)->value.name.v[1], (self)->value.name.v[2])

//======================================================================
// generic shader uniform update access macro (float4)
#define shader_updatef4(self,name) \
  glUniform4fARB((self)->uniform.name, (self)->value.name.v[0], (self)->value.name.v[1], (self)->value.name.v[2], (self)->value.name.v[3])

//======================================================================
// generic shader uniform update access macro (float4v)
#define shader_updatef4v(self,name, count) \
  glUniform4fvARB((self)->uniform.name, count, &((self)->value.name[0].v[0]))

//======================================================================
// generic shader uniform update access macro (float2v)
#define shader_updatef2v(self,name, count) \
  glUniform2fvARB((self)->uniform.name, count, &((self)->value.name[0].v[0]))

//======================================================================
// generic shader initialization
struct shader *shader_init(
  struct shader *shader, const char *vert, const char *frag
);

#endif

