/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Output Module
===================================================================== */

#include <math.h>
#include <stdio.h>

#include "config.h"
#include "output.h"

struct output *output_init(struct output *output) {
  if (!output) { return 0; }
  if (! minmax_init(&output->minmax)) { return 0; }
  if (! stretch_init(&output->stretch)) { return 0; }
  output->frame = 0;
  output->width = 1;
  output->height = 1;
  output->winwidth = 1;
  output->winheight = 1;
  glGenTextures(1, &output->texture);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, output->texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, config_win_width, config_win_height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0); // FIXME
  glDisable(GL_TEXTURE_2D);
  return output;
}

void output_reshape(struct output *output, int ww, int hh, int w, int h) {
  minmax_reshape(&output->minmax, ww, hh);
  stretch_reshape(&output->stretch, ww, hh);
  output->width = ww;
  output->height = hh;
  output->winwidth = w;
  output->winheight = h;
}

void output_display(struct output *output, GLuint texture, GLuint fbo) {
  minmax_display(&output->minmax, fbo, texture);
  stretch_display(&output->stretch, fbo, texture, &output->minmax.minpixel[0], &output->minmax.maxpixel[0]);
  glClearColor(0,0,0,1);
  glClear(GL_COLOR_BUFFER_BIT);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, output->stretch.texture);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, output->winwidth, output->winheight);
  glBegin(GL_QUADS); {
    glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glDisable(GL_TEXTURE_2D);

  // FIXME
  output->frame += 1;
}

// EOF
