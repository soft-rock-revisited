#version 120
precision highp float;

uniform sampler2D tex;
uniform vec3 sub;
uniform vec3 mul;

void main() {
  vec3 t = texture2D(tex, gl_TexCoord[0].xy).rgb;
  gl_FragData[0] = vec4(mix(t, (t - sub) * mul, 0.25), 1.0);
}
