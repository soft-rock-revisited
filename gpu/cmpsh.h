#ifndef CMPSH_H
#define CMPSH_H 1

#include "config.h"
#include "shader.h"

struct cmpsh { struct shader shader;
  struct { GLint tex0, tex1, tex2, val; } uniform;
  struct { int tex0, tex1, tex2; struct vec4 val; } value;
  GLuint tex[2];
  int which;
  int width;
  int height;
};

struct cmpsh *cmpsh_init(struct cmpsh *cmpsh);
void cmpsh_reset(struct cmpsh *cmpsh);
GLuint cmpsh_display(struct cmpsh *cmpsh, GLuint fbo, GLuint tex1, GLuint tex2, struct vec4 *val);

#endif
