#include <math.h>
#include "util.h"
#include "hsv.h"

void hsv2rgb(struct vec3 *hsv, struct vec3 *rgb) {
  float r=0, g=0, b=0;
  float h = clamp(hsv->v[0], 0.0, 1.0);
  float s = clamp(hsv->v[1], 0.0, 1.0);
  float v = clamp(hsv->v[2], 0.0, 1.0);
  h *= 360.f; // convert hue to degrees
  if (s == 0.0) { // black and white
    r = g = b = v;
  } else {
    if (h == 360.0) { h = 0; } // 360 == 0 degrees
    h /= 60.0f; // hue is now [0, 6]
    {
      int i = (int)floor(h);
      float f = h - i; // f is the fractional part of h
      float p = v * (1 - s);
      float q = v * (1 - s * f);
      float t = v * (1 - s * (1 - f));
      switch (i) {
      case 0: r = v; g = t; b = p; break;
      case 1: r = q; g = v; b = p; break;
      case 2: r = p; g = v; b = t; break;
      case 3: r = p; g = q; b = v; break;
      case 4: r = t; g = p; b = v; break;
      case 5: r = v; g = p; b = q; break;
      }
    }
  }
  rgb->v[0] = r;
  rgb->v[1] = g;
  rgb->v[2] = b;
}
