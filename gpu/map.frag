#version 120
precision highp float;

uniform sampler2D phases_cur;
uniform sampler2D phases_old;
uniform float sr;
uniform float note_lo;
uniform float note_hi;
uniform float modi_lo;
uniform float modi_hi;

float mtof(float f) {
  return 8.17579891564 * exp(0.0577622650 * f);
}

float ftom(float f) {
  return log(f / 8.17579891564) / 0.0577622650;
}

vec2 mtof(vec2 v) {
  return vec2(mtof(v.x), mtof(v.y));
}

void main() {
  vec2 param = gl_TexCoord[0].xy;
  float note = mix(note_lo, note_hi, param.x);
  float modi = mix(modi_lo, modi_hi, param.y);
  vec2 phase_cur = texture2D(phases_cur, gl_TexCoord[0].xy).xy;
  vec2 phase_old = texture2D(phases_old, gl_TexCoord[0].xy).xy;
  vec2 p = phase_cur + mtof(note + modi * cos(6.283185307179586 * phase_old.yx)) / sr;
  vec2 phase_out = p - floor(p);
  gl_FragData[0] = vec4(phase_out, vec2(0.0));
}
