#version 120
precision highp float;

uniform sampler2D statistic;
uniform float sr;

float ftoo(float f) {
  return log2(f);
}

void main() {
  mat3 yuv2rgb = mat3(1.0, 1.407, 0.0, 1.0, -0.677, -0.236, 1.0, 0.0, 1.848);
  vec4 s = texture2D(statistic, gl_TexCoord[0].xy);
  float mean = s.y / s.x;
  float stddev = sqrt(s.x * s.z - s.y * s.y) / s.x;
  if (! (stddev > 0.0)) {
    stddev = 0.0;
  }
  float hue = 6.283185307179586 * mean / 12.0;
  float value = max(0.0, 0.5 - stddev / 48.0);
  float satur = 0.25 * value; //clamp(0.25 * log2(mean / harm), value / 16.0, value / 2.0);
  vec3 yuv = vec3(value, satur * sin(hue), satur * cos(hue));
  vec3 rgb = yuv * yuv2rgb;
  gl_FragData[0] = vec4(clamp(rgb, 0.0, 1.0), 1.0);
}
