#ifndef AUDIO_H
#define AUDIO_H 1

#include <stdio.h>
#include <jack/jack.h>

#include "config.h"
#include "scope.h"
#include "zoomer.h"

struct audio {
  struct zoomer *zoomer;
  struct scope *scope;
  double samples; // total samples scanned
  jack_client_t *client;
  jack_port_t *port[2];
  double sphase;
  int sr;
  float phase[config_oscillators][2];
  float osc[config_oscillators][2][config_blocksize];
  int oscn;
  double bphase;
  double blength;
  float x[2], x1[2], y1[2];
  int scope_phase;
};

struct audio *audio_init(struct audio *audio, struct zoomer *zoomer, struct scope *scope);
void audio_atexit(struct audio *audio);

#endif
