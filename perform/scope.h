#ifndef SCOPE_H
#define SCOPE_H 1

#include "shader.h"

#define SCOPE_SIZE 512
// 60fps at 48kHz
#define SCOPE_INTERVAL 800

#define SCOPE_WIN_X 512
#define SCOPE_WIN_Y 0
#define SCOPE_WIN_SIZE 512

struct scope { struct shader shader;
  struct { GLint tex0, tex1; } uniform;
  struct { int tex0, tex1; } value;
  GLuint tex[2];
  float buf[2][SCOPE_SIZE];
};

struct scope *scope_init(struct scope *scope);
void scope_display(struct scope *scope);

#endif
