#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>

#include "scope.h"
#include "scope.frag.c"
#include "util.h"

struct scope *scope_init(struct scope *scope) {
  if (! scope) { return 0; }
  if (! shader_init(&scope->shader, 0, scope_frag)) { return 0; }
  shader_uniform(scope, tex0);
  shader_uniform(scope, tex1);
  scope->value.tex0 = 0;
  scope->value.tex1 = 1;
  glGenTextures(2, &scope->tex[0]);
  for (int i = 0; i < 2; ++i) {
    glBindTexture(GL_TEXTURE_1D, scope->tex[i]);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, SCOPE_SIZE, 0, GL_RED, GL_FLOAT, &scope->buf[0][0]);
    glBindTexture(GL_TEXTURE_1D, 0);
  }
  return scope;
}

void scope_display(struct scope *scope) {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(SCOPE_WIN_X, SCOPE_WIN_Y, SCOPE_WIN_SIZE, SCOPE_WIN_SIZE);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_1D, scope->tex[1]);
  glTexSubImage1D(GL_TEXTURE_1D, 0, 0, SCOPE_SIZE, GL_RED, GL_FLOAT, &scope->buf[1][0]);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_1D, scope->tex[0]);
  glTexSubImage1D(GL_TEXTURE_1D, 0, 0, SCOPE_SIZE, GL_RED, GL_FLOAT, &scope->buf[0][0]);
  glUseProgramObjectARB(scope->shader.program);
  shader_updatei(scope, tex0);
  shader_updatei(scope, tex1);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glUseProgramObjectARB(0);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_1D, 0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_1D, 0);
}
