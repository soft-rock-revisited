#ifndef REVISITED_H
#define REVISITED_H 1

#include <time.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include "config.h"
#include "audio.h"
#include "scope.h"
#include "zoomer.h"

//======================================================================
// main module data
struct revisited {
  struct audio audio;
  struct scope scope;
  struct zoomer zoomer;
  time_t starttime;
  unsigned int frame;
  int fullscreen;
};

//======================================================================
// prototypes
int revisited_init(void);
void revisited_reshape(int w, int h);
void revisited_display(void);
void revisited_atexit(void);
void revisited_idle(void);
void revisited_keynormal(unsigned char key, int x, int y);
void revisited_keyspecial(int key, int x, int y);
void revisited_mouse(int button, int state, int x, int y);
void revisited_motion(int x, int y);

#endif
