#version 120
precision highp float;

uniform sampler1D tex0;
uniform sampler1D tex1;

void main() {
  vec2 p = gl_TexCoord[0].xy;
  float a = texture1D(tex0, p.x).x;
  float b = texture1D(tex1, p.y).x;
  vec3 yuv = vec3(sqrt(abs(a * b)), 0.5 * a, 0.5 * b);
  vec3 rgb = yuv * mat3(1.0, 1.407, 0.0, 1.0, -0.677, -0.236, 1.0, 0.0, 1.848);
  gl_FragColor = vec4(clamp(rgb, vec3(0.0), vec3(1.0)), 0.0);
}
