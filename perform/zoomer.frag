#version 120
precision highp float;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform float zoom;
uniform float blend;

void main() {
  vec2 p = vec2(0.5) + zoom * (gl_TexCoord[0].xy - vec2(0.5));
  vec2 q = vec2(0.5) + 0.5 * (p - vec2(0.5));
  gl_FragColor = mix(texture2D(tex0, q), texture2D(tex1, p), blend);
}
