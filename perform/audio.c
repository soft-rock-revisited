/* =====================================================================
soft-rock-revisited
Copyright (C) 2010-2012  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Audio output
===================================================================== */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "config.h"
#include "audio.h"

/* {{{ copy/pasted from pd-0.42, hardcoded little-endian {{{ */

#define UNITBIT32 1572864.  /* 3*2^19; bit 32 has place value 1 */
#define HIOFFSET 1                                                              
#define LOWOFFSET 0                                                             
#define int32 int32_t
#define LOGCOSTABSIZE 9
#define COSTABSIZE (1<<LOGCOSTABSIZE)

float cos_table[COSTABSIZE+1];

union tabfudge
{
    double tf_d;
    int32 tf_i[2];
};

static inline float costab(float p) {
        union tabfudge tf;
        double dphase = (double)(p * (float)(COSTABSIZE)) + UNITBIT32;
        tf.tf_d = UNITBIT32;
        int normhipart = tf.tf_i[HIOFFSET];
        tf.tf_d = dphase;
        float *addr = cos_table + (tf.tf_i[HIOFFSET] & (COSTABSIZE-1));
        tf.tf_i[HIOFFSET] = normhipart;
        float frac = tf.tf_d - UNITBIT32;
        float f1 = addr[0];
        float f2 = addr[1];
        return f1 + frac * (f2 - f1);
}

static void cos_maketable(void)
{
    int i;
    float *fp, phase, phsinc = (2. * 3.14159) / COSTABSIZE;
    for (i = COSTABSIZE + 1, fp = cos_table, phase = 0; i--;
        fp++, phase += phsinc)
            *fp = cos(phase);
}

/* }}} copy/pasted from pd-0.42 }}} */

static inline float mtoff(float f) {
  return 8.17579891564f * expf(0.0577622650f * f);
}

void audio_compute(struct audio *audio, jack_default_audio_sample_t **out, jack_nframes_t nframes) {
  float sr = audio->sr;
  float ophase[config_oscillators][2];
  float phase[config_oscillators][2];
  float note[config_oscillators];
  float modi[config_oscillators];
  note[0] = audio->zoomer->note;
  modi[0] = audio->zoomer->modi;
  float nradius = audio->zoomer->noter / ZOOMER_WIN_SIZE;
  float mradius = audio->zoomer->modir / ZOOMER_WIN_SIZE;
  for (int o = 1; o < config_oscillators; ++o) {
    note[o] = note[0] + nradius * cos(o * 6.283185307179586 / (config_oscillators - 1));
    modi[o] = modi[0] + mradius * sin(o * 6.283185307179586 / (config_oscillators - 1));
  }
  float pan[config_oscillators][2];
  for (int o = 0; o < config_oscillators; ++o) {
    phase[o][0] = audio->phase[o][0];
    phase[o][1] = audio->phase[o][1];
    if (o == 0) {
      pan[o][1] = sqrtf(0.5);
    } else {
      pan[o][1] = cosf(6.283185307179586f * 0.125 * (0.9 * cosf((o + 0.5) * 6.283185307179586f / (config_oscillators - 1)) + 1.0));
    }
    pan[o][0] = sqrtf(1 - pan[o][1] * pan[o][1]);
    pan[o][0] *= (o == 0 ? 0.5 : (0.5 / (config_oscillators - 1)));
    pan[o][1] *= (o == 0 ? 0.5 : (0.5 / (config_oscillators - 1)));
  }
  for (jack_nframes_t n = 0; n < nframes; ++n) {
    out[0][n] = 0.0;
    out[1][n] = 0.0;
    // FM oscillators
    for (int o = 0; o < config_oscillators; ++o) {
      audio->osc[o][0][audio->oscn] = costab(phase[o][0]);
      audio->osc[o][1][audio->oscn] = costab(phase[o][1]);
      for (int i = 0; i < 2; ++i) {
        int j = 1 - i;
        float p = phase[o][i] + mtoff(note[o] + modi[o] * audio->osc[o][j][(audio->oscn + 1) % config_blocksize]) / sr;
        ophase[o][i] = p - floorf(p);
      }
      for (int i = 0; i < 2; ++i) {
        phase[o][i] = ophase[o][i];
        out[i][n] += pan[o][i] * audio->osc[o][i][audio->oscn];
      }
    }
    // DC blocker
    for (int i = 0; i < 2; ++i) {
      audio->x1[i] = audio->x[i];
      audio->x[i] = out[i][n];
      out[i][n] = audio->x[i] - audio->x1[i] + 0.999 * audio->y1[i];
      audio->y1[i] = out[i][n];
      if (audio->scope_phase < SCOPE_SIZE) {
        audio->scope->buf[i][audio->scope_phase] = out[i][n];
      }
    }
    audio->scope_phase = (audio->scope_phase + 1) % SCOPE_INTERVAL;
    // feedback delay
    audio->oscn = (audio->oscn + 1) % config_blocksize;
  }
  // update state
  audio->sphase += nframes / (double) audio->sr;
  for (int o = 0; o < config_oscillators; ++o) {
    audio->phase[o][0] = phase[o][0];
    audio->phase[o][1] = phase[o][1];
  }
}

/* JACK audio processing callback */
static int audio_process(jack_nframes_t nframes, void *arg) {
  struct audio *audio = arg;
  /* get JACK buffers */
  jack_default_audio_sample_t *out[2];
  out[0] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(audio->port[0], nframes);
  out[1] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(audio->port[1], nframes);
  audio_compute(audio, out, nframes);
  return 0;
}

/* JACK sample rate callback */
static int audio_srate(jack_nframes_t sr, void *arg) {
  struct audio *audio = arg;
  if ((jack_nframes_t) audio->sr != sr) {
    fprintf(stderr, "WARN: JACK sample rate: %d != %d\n", sr, audio->sr);
  }
  return 0;
}

/* JACK error callback */
static void audio_error(const char *desc) {
  fprintf(stderr, "JACK error: %s\n", desc);
}

/* JACK shutdown callback */
static void audio_shutdown(void *arg) {
  fprintf(stderr, "JACK shutdown\n");
}

/* JACK timebase callback */
static void audio_timebase(jack_transport_state_t state, jack_nframes_t nframes, jack_position_t *pos, int new_pos, void *arg) {
  //struct audio *audio = arg;
  if (new_pos) {
    pos->valid = JackPositionBBT;
    pos->beats_per_bar = 4;
    pos->beat_type = 0.25;
    pos->ticks_per_beat = 1920;
    pos->beats_per_minute = 120;
    pos->bar = 1;
    pos->beat = 1;
    pos->tick = 0;
    pos->bar_start_tick = (pos->bar - 1) * pos->beats_per_bar * pos->ticks_per_beat;
  } else {
    pos->tick +=nframes * pos->ticks_per_beat * pos->beats_per_minute / (pos->frame_rate * 60);
    while (pos->tick >= pos->ticks_per_beat) {
      pos->tick -= pos->ticks_per_beat;
      if (++pos->beat > pos->beats_per_bar) {
        pos->beat = 1;
        ++pos->bar;
        pos->bar_start_tick += pos->beats_per_bar * pos->ticks_per_beat;
      }
    }
  }
}

/* exit callback */
void audio_atexit(struct audio *audio) {
  jack_client_close(audio->client);
}

//======================================================================
// ...
struct audio *audio_init(struct audio *audio, struct zoomer *zoomer, struct scope *scope) {
  if (! audio) { return 0; }
  cos_maketable();
  audio->zoomer = zoomer;
  audio->scope = scope;
  audio->scope_phase = 0;
  audio->samples = 0;
  for (int o = 0; o < config_oscillators; ++o) {
    audio->phase[o][0] = 0;
    audio->phase[o][1] = 0;
  }
  audio->oscn = 0;
  audio->sr = config_samplerate;
  audio->sphase = 0;
  audio->bphase = 0;
  audio->blength = audio->sr * 60.0 / config_bpm;
  jack_set_error_function(audio_error);
  if (!(audio->client = jack_client_new("srrv"))) {
    fprintf (stderr, "JACK server not running?\n");
  } else {
    jack_set_process_callback(audio->client, audio_process, audio);
    jack_set_sample_rate_callback(audio->client, audio_srate, audio);
    jack_on_shutdown(audio->client, audio_shutdown, audio);
    /* create ports */
    audio->port[0] = jack_port_register(
      audio->client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
    );
    audio->port[1] = jack_port_register(
      audio->client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
    );
    /* activate audio */
    if (jack_activate(audio->client)) {
      fprintf (stderr, "cannot activate JACK client\n");
    } else {
      /* must be activated before connecting JACK ports */
      const char **ports;
      if ((ports = jack_get_ports(
        audio->client, NULL, NULL, JackPortIsPhysical | JackPortIsInput
      ))) {
        /* connect up to two physical playback ports */
        int i = 0;
        while (ports[i] && i < 2) {
          if (jack_connect(
            audio->client, jack_port_name(audio->port[i]), ports[i]
          )) {
            fprintf(stderr, "cannot connect JACK output port\n");
          }
          i++;
        }
        free(ports);
      }
      /* become timebase master */
      if (jack_set_timebase_callback(audio->client, 0, audio_timebase, audio)) {
        fprintf(stderr, "cannot become JACK master\n");
      }
      jack_transport_start(audio->client);
    }
  }
  return audio;
}

// EOF
