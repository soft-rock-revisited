#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>

#include "zoomer.h"
#include "zoomer.frag.c"
#include "util.h"

struct zoomer *zoomer_init(struct zoomer *zoomer, const char *datafile, double nsize, double msize, double cnote, double cmodi) {
  if (! zoomer) { return 0; }
  if (! shader_init(&zoomer->shader, 0, zoomer_frag)) { return 0; }
  shader_uniform(zoomer, tex0);
  shader_uniform(zoomer, tex1);
  shader_uniform(zoomer, zoom);
  shader_uniform(zoomer, blend);
  zoomer->value.tex0 = 0;
  zoomer->value.tex1 = 1;
  zoomer->phase = 0;
  zoomer->speed = 0;
  zoomer->nsize = nsize;
  zoomer->msize = msize;
  zoomer->cnote = cnote;
  zoomer->cmodi = cmodi;
  zoomer->note = cnote;
  zoomer->modi = cmodi;
  zoomer->mousex = ZOOMER_WIN_SIZE / 2;
  zoomer->mousey = ZOOMER_WIN_SIZE / 2;
  struct zoomer_pixels *pixels = calloc(1, sizeof(struct zoomer_pixels));
  if (! pixels) { return 0; }
  FILE *f = fopen(datafile, "rb");
  if (! f) { return 0; }
  if (1 != fread(&pixels->pixels[0][0][0][0], sizeof(struct zoomer_pixels), 1, f)) { return 0; }
  fclose(f);
  glGenTextures(ZOOMER_COUNT, &zoomer->tex[0]);
  for (int i = 0; i < ZOOMER_COUNT; ++i) {
    glBindTexture(GL_TEXTURE_2D, zoomer->tex[i]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ZOOMER_SIZE, ZOOMER_SIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, &pixels->pixels[i][0][0][0]);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  free(pixels);
  return zoomer;
}

void zoomer_display(struct zoomer *zoomer) {
  zoomer->phase += zoomer->speed;
  int w = floor(clamp(zoomer->phase, 0.0, ZOOMER_COUNT - 2.0));
  double s = pow(0.5, zoomer->phase);
  double t = zoomer->phase - w;
  zoomer->value.zoom = pow(0.5, t);
  zoomer->value.blend = (1 - pow(0.25, t)) / (pow(0.25, t - 1) - pow(0.25, t));
  zoomer->noter = zoomer->nsize * s;
  zoomer->modir = zoomer->msize * s;
  zoomer->note = zoomer->noter * 2.0 * (zoomer->mousey / (double) ZOOMER_WIN_SIZE - 0.5) + zoomer->cnote;
  zoomer->modi = zoomer->modir * 2.0 * (zoomer->mousex / (double) ZOOMER_WIN_SIZE - 0.5) + zoomer->cmodi;
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(ZOOMER_WIN_X, ZOOMER_WIN_Y, ZOOMER_WIN_SIZE, ZOOMER_WIN_SIZE);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, zoomer->tex[w + 1]);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, zoomer->tex[w]);
  glUseProgramObjectARB(zoomer->shader.program);
  shader_updatei(zoomer, tex0);
  shader_updatei(zoomer, tex1);
  shader_updatef(zoomer, zoom);
  shader_updatef(zoomer, blend);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 1);
    glTexCoord2f(1, 0); glVertex2f(1, 1);
    glTexCoord2f(1, 1); glVertex2f(1, 0);
    glTexCoord2f(0, 1); glVertex2f(0, 0);
  } glEnd();
  glUseProgramObjectARB(0);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, 0);
}

int zoomer_key(struct zoomer *zoomer, unsigned char key) {
  if (key == ' ') {
    zoomer->speed = ZOOMER_COUNT / (15.0 * 60 * 60);
    return 1;
  }
  return 0;
}

int zoomer_mouse(struct zoomer *zoomer, int x, int y) {
  if (ZOOMER_WIN_X <= x && x < ZOOMER_WIN_X + ZOOMER_WIN_SIZE &&
       ZOOMER_WIN_Y <= y && y < ZOOMER_WIN_Y + ZOOMER_WIN_SIZE) {
    zoomer->mousex = x - ZOOMER_WIN_X;
    zoomer->mousey = y - ZOOMER_WIN_Y;
    return 1;
  }
  return 0;
}
