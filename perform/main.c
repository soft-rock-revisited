/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Main Program Entry Point
===================================================================== */

#include <stdio.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include "revisited.h"
#include "config.h"

int main(int argc, char **argv) {
  /* initialize GLUT etc */
  fprintf(stderr, "soft-rock-revisited (GPL) 2012 Claude Heiland-Allen <claude@mathr.co.uk>\n");
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutInitWindowSize(config_win_width, config_win_height);
  glutInit(&argc, argv);
  glutCreateWindow("srrv");
  glewInit();
  glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);
  glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
  /* activate callbacks and enter main loop */
  if (revisited_init()) {
    glutDisplayFunc(revisited_display);
    glutReshapeFunc(revisited_reshape);
    glutIdleFunc(revisited_idle);
    glutKeyboardFunc(revisited_keynormal);
    glutSpecialFunc(revisited_keyspecial);
    glutMouseFunc(revisited_mouse);
    glutMotionFunc(revisited_motion);
    glutPassiveMotionFunc(revisited_motion);
    glutSetCursor(GLUT_CURSOR_CROSSHAIR);
    glutMainLoop();
    return 0; // never reached
  } else {
    return 1;
  }
}
