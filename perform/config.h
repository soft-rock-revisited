/* =====================================================================
soft-rock-revisited
Copyright (C) 2008-2012  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------
Configuration
===================================================================== */

#ifndef CONFIG_H
#define CONFIG_H 1

// =====================================================================
// expert settings
#define config_aspect         1
#define config_tex_width    256
#define config_tex_height   256
#define config_win_width   1024
#define config_win_height   512
#define config_scale_factor   1
#define config_samplerate 48000
#define config_oscillators    5
#define config_beats          1
#define config_bpm           60
#define config_overdrive      8
#define config_blocksize     32

#endif
