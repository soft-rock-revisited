#ifndef ZOOMER_H
#define ZOOMER_H 1

#include "shader.h"

#define ZOOMER_WIN_X 0
#define ZOOMER_WIN_Y 0
#define ZOOMER_WIN_SIZE 512
#define ZOOMER_SIZE 128
#define ZOOMER_COUNT 16

struct zoomer_pixels {
  unsigned char pixels[ZOOMER_COUNT][ZOOMER_SIZE][ZOOMER_SIZE][3];
};

struct zoomer { struct shader shader;
  struct { GLint tex0, tex1, zoom, blend; } uniform;
  struct { int tex0, tex1; float zoom, blend; } value;
  GLuint tex[ZOOMER_COUNT];
  double phase;
  double speed;
  double nsize;
  double msize;
  double cnote;
  double cmodi;
  int mousex;
  int mousey;
  double note;
  double modi;
  double noter;
  double modir;
};

struct zoomer *zoomer_init(struct zoomer *zoomer, const char *datafile, double nsize, double msize, double cnote, double cmodi);
void zoomer_display(struct zoomer *zoomer);
int zoomer_key(struct zoomer *zoomer, unsigned char key);
int zoomer_mouse(struct zoomer *zoomer, int x, int y);

#endif
