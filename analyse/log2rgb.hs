import qualified Data.ByteString as BS
import Data.Fixed (mod')
import Data.Word (Word8)
import System.Environment (getArgs)

hsv2rgb :: Double -> Double -> Double -> [Double]
hsv2rgb h 0 v = [v, v, v]
hsv2rgb 1 s v = hsv2rgb 0 s v
hsv2rgb h s v =
  case i of
    0 -> [v, t, p]
    1 -> [q, v, p]
    2 -> [p, v, t]
    3 -> [p, q, v]
    4 -> [t, p, v]
    5 -> [v, p, q]
  where
    i = floor h'
    h' = 6 * (h - fromIntegral (floor h))
    f = h' - fromIntegral i
    p = v * (1 - s)
    q = v * (1 - s * f)
    t = v * (1 - s * (1 - f))

d2b :: Double -> Word8
d2b x = round (255 * x)

go :: [String] -> [Double]
go ["image:", _, _, "pitched", _, _, x, _] = hsv2rgb ((read x / 12) `mod'` 1) 1 1
go ["image:", _, _, "chaotic", _, _, x, _] = hsv2rgb ((read x / 12) `mod'` 1) 0.5 0.5
go _ = []

main :: IO ()
main = do
  [inFile, outFile] <- getArgs
  BS.writeFile outFile . BS.pack . map d2b . concatMap go . map words . lines =<< readFile inFile
