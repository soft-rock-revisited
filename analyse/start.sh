#!/bin/bash
#
# suggested usage (for a quad-core, launching 3 batches):
#
# $ ./start.sh ~/opt/pd-0.42/bin/pd -jack 3 5000 | ts > start.sh.log
#
# on my desktop with 3 cores doing batch work I get ~50 points per second
# on my laptop  with 1 cores doing batch work I get ~12 points per second
#
PD="${1}"
API="${2}"
COUNT="${3}"
PORT="${4}"
if [ "x${PD}" = "x" ]
then
  PD="$(which pd)"
fi
if [ "x${API}" = "x" ]
then
  API="-alsa"
fi
if [ "x${COUNT}" = "x" ]
then
  COUNT="1"
fi
if [ "x${PORT}" = "x" ]
then
  PORT="5000"
fi
for i in $(seq 1 "${COUNT}")
do
  "${PD}" -nrt -r 48000 -batch -nosound -open loader.pd -send "; port $((PORT + i))" 2>&1 &
done
sleep 5
"${PD}" -noadc -rt -r 48000 "${API}" -channels 2 -path "$(dirname "${PD}")/../lib/pd/extra/Gem":"$(dirname "${PD}")/../lib/pd/extra/lua" -lib Gem:lua -open controllers.pd  -send "; batch ${COUNT} $((PORT + 1))" -stderr 2>&1 &
wait
