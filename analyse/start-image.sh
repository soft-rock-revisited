#!/bin/bash
#
# suggested usage (for a quad-core, launching 3 batches):
#
# $ ./start.sh ~/opt/pd-0.42/bin/pd -jack 3 5000 | ts > start.sh.log
#
# on my desktop with 3 cores doing batch work I get ~50 points per second
# on my laptop  with 1 cores doing batch work I get ~12 points per second
#
PD="${1}"
WIDTH="${2}"
HEIGHT="${3}"
NOTE="${4}"
MODI="${5}"
SIZE="${6}"
if [ "x${PD}" = "x" ]
then
  PD="$(which pd)"
fi
if [ "x${WIDTH}" = "x" ]
then
  WIDTH="256"
fi
if [ "x${HEIGHT}" = "x" ]
then
  HEIGHT="144"
fi
if [ "x${NOTE}" = "x" ]
then
  NOTE="36"
fi
if [ "x${MODI}" = "x" ]
then
  MODI="57.875"
fi
if [ "x${SIZE}" = "x" ]
then
  SIZE="12"
fi
"${PD}" -stderr -nrt -r 48000 -batch -nosound -open loader-image.pd -send "; go ${WIDTH} ${HEIGHT} ${NOTE} ${MODI} ${SIZE}" >"${WIDTH}x${HEIGHT}_${NOTE}x${MODI}_${SIZE}.log" 2>&1
