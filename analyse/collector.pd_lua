local C = pd.Class:new():register("collector")
local CC = { }

function C:initialize(sel, atoms)
  if type(atoms[1]) ~= "string" then
    self:error("expected symbol for first argument")
    return false
  end
  if type(atoms[2]) ~= "number" then
    self:error("expected number for second argument")
    return false
  end
  if type(CC[atoms[1]]) ~= "table" then
    CC[atoms[1]] = { }
  end
  self.cc = CC[atoms[1]]
  self.split = atoms[2]
  self.inlets = 2
  self.outlets = 3
  return true
end

function C:in_2(sel, atoms)
  if (self.split + 1 < #atoms) then
    if type(self.cc[sel]) ~= "table" then
      self.cc[sel] = { }
    end
    local t = { }
    t.which  = sel
    t.input  = { }
    t.output = { }
    local i
    for i = 1,self.split+1 do
      t.input[i] = atoms[i]
    end
    for i = self.split+1,#atoms do
      t.output[i - self.split] = atoms[i]
    end
    self.cc[sel][#(self.cc[sel])+1] = t
  end
end

function C:in_1_bang()
  local col,t
  for col,s in pairs(self.cc) do
    self:outlet(3, "symbol", { col })
    for i,t in ipairs(s) do
      self:outlet(2, "list", t.input)
      self:outlet(1, "list", t.output)
    end
    self:outlet(3, "bang", {})
  end
end
